import React from 'react'
import links from '../links.js'
import categoryGroupBuckets from '../categories.js'
import alphabetGroupBuckets from '../alphabets.js'

const LinksByCategory = category => {
    return (
      links.filter(link => link.category === category.category)
      .sort((a, b) => (a.title.toLowerCase() > b.title.toLowerCase()) ? 1 : -1)
      .map(link => <div key={link.title}><a href={link.url} rel="noopener noreferrer" target="_blank">{link.title}</a><br/></div>)
    )
  }
  
  const LinksByAlphabet = category => {
    return (
      links.filter(link => link.title[0] === category.category)
      .sort((a, b) => (a.title.toLowerCase() > b.title.toLowerCase()) ? 1 : -1)
      .map(link => <div key={link.title}><a href={link.url} rel="noopener noreferrer" target="_blank">{link.title}</a><br/></div>)
    )
  }
  
  const CategoryColumns = ({bucket}) => {
    return (
      <div className="column">
        {categoryGroupBuckets[bucket].map(group => {
          console.log(group[0].category)
          return (
            <div key={group[0].category}><h2>{group[0].category.toUpperCase()}</h2><LinksByCategory category={group[0].category}/></div>
          )}
        )}
      </div>
    )
  }
  
  const AlphabetColumns = ({bucket}) => {
    return (
      <div className="column">
        {alphabetGroupBuckets[bucket].map(group => {
          console.log(group[0].title[0])
          return (
            <div key={group[0].title[0].toUpperCase()}><h2>{group[0].title[0].toUpperCase()}</h2><LinksByAlphabet category={group[0].title[0].toUpperCase()}/></div>
          )}
        )}
      </div>
    )
  }
  
  const UpperHeader = ({buttonText, setButtonText, arrangeMode, setarrangeMode}) => {
  
    const arrangeFunction = () => {
      if (arrangeMode === "categories") {
        setButtonText("Järjestä kategorioittain")
        setarrangeMode("alphabets")
        document.cookie = "mode=alphabets; samesite=strict; expires=Thu, 1 Jan 2099 00:00:00 UTC";
      } else if (arrangeMode === "alphabets") {
        setButtonText("Järjestä aakkosittain")
        setarrangeMode("categories")   
        document.cookie = "mode=categories; samesite=strict; expires=Thu, 1 Jan 2099 00:00:00 UTC";
      }
    }
  
    return (
      <div className="header">
        <button className="arrangeButton" onClick={arrangeFunction}>{buttonText}</button>
      </div>
    )
  }
  
  const CategoryLinks = () => {
    return (
      <div className="row">
        <CategoryColumns bucket={0} />
        <CategoryColumns bucket={1} />
        <CategoryColumns bucket={2} />
        <CategoryColumns bucket={3} />
      </div> 
    )
  }
  
  const AlphabetLinks = () => {
    return(  
      <div className="row">
        <AlphabetColumns bucket={0} />
        <AlphabetColumns bucket={1} />
        <AlphabetColumns bucket={2} />
        <AlphabetColumns bucket={3} />
      </div>  
    )
  }
  
  const BottomHeader = () => {
    return (
      <div className="header"><h5>Coding & design: JouHou </h5></div>
    )
  }

  export {UpperHeader, CategoryLinks, AlphabetLinks, BottomHeader}