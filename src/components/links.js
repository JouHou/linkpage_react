const links = [
  {
    "title": "Luontoon.fi",
    "url": "https://www.luontoon.fi/",
    "category": "liikkuminen"
  },
  {
    "title": "Waltti",
    "url": "https://kauppa.waltti.fi/en/",
    "category": "muu tärkiä"
  },
  {
    "title": "MTV",
    "url": "https://www.mtv.fi/",
    "category": "elävä kuva"
  },
  {
    "title": "Taivaanvahti",
    "url": "https://www.taivaanvahti.fi/",
    "category": "avaruussää"
  },
  {
    "title": "picoCTF",
    "url": "https://picoctf.com/",
    "category": "tekniikka"
  },
  {
    "title": "YLE",
    "url": "https://yle.fi",
    "category": "uutiset"
  },
  {
    "title": "Patikka.net",
    "url": "http://www.patikka.net/paasivu.htm",
    "category": "liikkuminen"
  },
  {
    "title": "FMI revontulet ja avaruussää",
    "url": "https://www.ilmatieteenlaitos.fi/revontulet-ja-avaruussaa",
    "category": "avaruussää"
  },
  {
    "title": "Elisa Viihde",
    "url": "https://elisaviihde.fi/",
    "category": "elävä kuva"
  },
  {
    "title": "Tulikartta",
    "url": "https://www.tulikartta.fi/index.php?type=Pohjois-Pohjanmaa&lataus=1",
    "category": "liikkuminen"
  },
  {
    "title": "Weboodi",
    "url": "https://weboodi.oulu.fi/oodi/etusivu.html",
    "category": "koulu"
  },
  {
    "title": "YLE Areena",
    "url": "https://areena.yle.fi/tv",
    "category": "elävä kuva"
  },
  {
    "title": "Moodle",
    "url": "https://moodle.oulu.fi/",
    "category": "koulu"
  },
  {
    "title": "Enable Sysadmin",
    "url": "https://www.redhat.com/sysadmin/",
    "category": "tekniikka"
  },
  {
    "title": "Spaceweather",
    "url": "https://www.spaceweather.com/",
    "category": "avaruussää"
  },
  {
    "title": "Ulkoilublogit",
    "url": "https://ulkoilublogit.blogspot.com/",
    "category": "liikkuminen"
  },
  {
    "title": "Värityskuvia",
    "url": "http://www.supercoloring.com/fi",
    "category": "muu höpöhöpö"
  },
  {
    "title": "Retkikartta",
    "url": "https://www.retkikartta.fi",
    "category": "liikkuminen"
  },
  {
    "title": "Helsingin Sanomat",
    "url": "https://hs.fi",
    "category": "uutiset"
  },
  {
    "title": "GSM Arena",
    "url": "https://www.gsmarena.com/",
    "category": "tekniikka"
  },
  {
    "title": "Iltasanomat",
    "url": "https://is.fi",
    "category": "uutiset"
  },
  {
    "title": "Blockbuster",
    "url": "https://blockbuster.fi/",
    "category": "elävä kuva"
  },
  {
    "title": "Liiga",
    "url": "https://liiga.fi/fi/",
    "category": "muu höpöhöpö"
  },
  {
    "title": "PayPal",
    "url": "https://www.paypal.com/fi/webapps/mpp/home",
    "category": "muu tärkiä"
  },
  {
    "title": "Udemy",
    "url": "https://www.udemy.com/",
    "category": "tekniikka"
  },
  {
    "title": "Linux Today",
    "url": "https://www.linuxtoday.com/",
    "category": "tekniikka"
  },
  {
    "title": "Outi Kirjastot",
    "url": "https://outi.finna.fi/",
    "category": "muu tärkiä"
  },
  {
    "title": "overthewire",
    "url": "https://overthewire.org/wargames/",
    "category": "tekniikka"
  },
  {
    "title": "Ruutu",
    "url": "https://www.ruutu.fi/",
    "category": "elävä kuva"
  },
  {
    "title": "Phoronix",
    "url": "https://www.phoronix.com",
    "category": "tekniikka"
  },
  {
    "title": "Valosaastekartta 2",
    "url": "https://blue-marble.de/nightlights/2015",
    "category": "avaruussää"
  },
  {
    "title": "Youtube",
    "url": "https://www.youtube.com/feed/channels",
    "category": "elävä kuva"
  },
  {
    "title": "It's FOSS",
    "url": "https://itsfoss.com/",
    "category": "tekniikka"
  },
  {
    "title": "Päivyri",
    "url": "https://www.paivyri.fi/",
    "category": "muu tärkiä"
  },
  {
    "title": "Google Maps",
    "url": "https://www.google.fi/maps/",
    "category": "muu tärkiä"
  },
  {
    "title": "Hacker101",
    "url": "https://www.hacker101.com/",
    "category": "tekniikka"
  },
  {
    "title": "S-Pankki",
    "url": "https://www.s-pankki.fi",
    "category": "muu tärkiä"
  },
  {
    "title": "Valosaastekartta 1",
    "url": "http://darksitefinder.com/maps/world.html#4/39.00/-98.00",
    "category": "avaruussää"
  },
  {
    "title": "Whatsapp Web",
    "url": "https://web.whatsapp.com/",
    "category": "muu tärkiä"
  },
  {
    "title": "Foreca",
    "url": "https://foreca.fi",
    "category": "sää"
  },
  {
    "title": "Ilmatieteen Laitos",
    "url": "https://www.ilmatieteenlaitos.fi/saa/oulu/kumpulankangas",
    "category": "sää"
  },
  {
    "title": "Fedora Magazine",
    "url": "https://fedoramagazine.org/",
    "category": "tekniikka"
  },
  {
    "title": "Io-Tech",
    "url": "https://io-tech.fi",
    "category": "tekniikka"
  },
  {
    "title": "Geocaching.com",
    "url": "https://www.geocaching.com/play/search",
    "category": "liikkuminen"
  },
  {
    "title": "TechBBS lahjoitetaan",
    "url": "https://bbs.io-tech.fi/forums/lahjoitetaan.47/",
    "category": "tekniikka"
  },
  {
    "title": "The Register",
    "url": "https://www.theregister.com/",
    "category": "tekniikka"
  },
  {
    "title": "Humble Bundle",
    "url": "https://www.humblebundle.com/",
    "category": "tekniikka"
  },
  {
    "title": "Kaleva",
    "url": "https://www.kaleva.fi/",
    "category": "uutiset"
  },
  {
    "title": "Suomen Vesiputoukset",
    "url": "https://www.suomenvesiputoukset.fi/",
    "category": "liikkuminen"
  },
  {
    "title": "Kaleva Fingerpori",
    "url": "https://www.kaleva.fi/sarjakuvat/fingerpori",
    "category": "fingerpori"
  },
  {
    "title": "Oulun Joukkoliikenne",
    "url": "https://www.oulunjoukkoliikenne.fi/",
    "category": "muu tärkiä"
  },
  {
    "title": "Anandtech",
    "url": "https://anandtech.com",
    "category": "tekniikka"
  },
  {
    "title": "OMG!Ubuntu",
    "url": "https://www.omgubuntu.co.uk/",
    "category": "tekniikka"
  },
  {
    "title": "HS Fingerpori",
    "url": "https://hs.fi/fingerpori",
    "category": "fingerpori"
  },
  {
    "title": "YR.no",
    "url": "https://www.yr.no/place/Finland/Oulu/Kumpulankangas/",
    "category": "sää"
  },
  {
    "title": "Iltalehti",
    "url": "https://iltalehti.fi",
    "category": "uutiset"
  },
  {
    "title": "Aurora Forecast",
    "url": "https://www.gi.alaska.edu/monitors/aurora-forecast",
    "category": "avaruussää"
  },
  {
    "title": "Tori",
    "url": "https://www.tori.fi/pohjois-pohjanmaa?q=&cg=0&w=1&st=g&ca=2&l=0&md=th",
    "category": "muu tärkiä"
  },
  {
    "title": "Auringon nousu- ja laskuajat",
    "url": "http://www.wakkanet.fi/~jukka/aurinko.html",
    "category": "sää"
  }
]

export default links