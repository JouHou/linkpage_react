import links from './links.js'

console.log(links)
console.log("links length", links.length)

const alphabets = [...new Set(links.map(link => link.title[0].toUpperCase()))]
console.log("alphabets", alphabets)

var alphabetGroups = []
alphabets.forEach(category => {
  alphabetGroups.push(links.filter(link => link.title[0] === category))
})
alphabetGroups.sort((a,b) => a[0].title[0] > b[0].title[0] ? 1 : -1)
console.log("alphabetGroups", alphabetGroups)

var alphabetGroupBuckets = []
var temporaryAlphabetGroupBuckets = []

alphabetGroups.forEach(categoryGroup => {

    temporaryAlphabetGroupBuckets.push(categoryGroup)
    console.log("temporaryAlphabetGroupBuckets", temporaryAlphabetGroupBuckets)
    if (temporaryAlphabetGroupBuckets.reduce((acc, curr) =>  acc + curr.length, 0) >= Math.floor(links.length/4)) {
      console.log("over 4")
      alphabetGroupBuckets.push(temporaryAlphabetGroupBuckets)
      temporaryAlphabetGroupBuckets = []
    }
  
})

if (temporaryAlphabetGroupBuckets.length > 0) {
  alphabetGroupBuckets.push(temporaryAlphabetGroupBuckets)
}

console.log("alphabetGroupBuckets", alphabetGroupBuckets)

export default alphabetGroupBuckets