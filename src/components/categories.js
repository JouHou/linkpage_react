import links from './links.js'

console.log(links)
console.log("links length", links.length)

const categories = [...new Set(links.map(link => link.category))]
console.log("categories", categories)

var categoryGroups = []
categories.forEach(category => {
  categoryGroups.push(links.filter(link => link.category === category))
})
categoryGroups.sort((a,b) => a.length > b.length ? 1 : -1)
console.log("categoryGroups", categoryGroups)

var categoryGroupBuckets = []
var temporaryCategoryGroupBucket = []

categoryGroups.sort((a,b) => a.length > b.length ? 1 : -1)
categoryGroups.forEach(categoryGroup => {

  if (categoryGroup.length >= Math.floor(links.length/4)) {
    categoryGroupBuckets.push([])
    categoryGroupBuckets[categoryGroupBuckets.length-1].push(categoryGroup)
  } else {
    temporaryCategoryGroupBucket.push(categoryGroup)
    console.log("temporaryCategoryGroupBucket", temporaryCategoryGroupBucket)
    if (temporaryCategoryGroupBucket.reduce((acc, curr) =>  acc + curr.length, 0) >= Math.floor(links.length/5)) {
      console.log("over 4")
      categoryGroupBuckets.push(temporaryCategoryGroupBucket)
      temporaryCategoryGroupBucket = []
    }
  }
})

if (temporaryCategoryGroupBucket.length > 0) {
  categoryGroupBuckets.push(temporaryCategoryGroupBucket)
}

console.log("categoryGroupBuckets", categoryGroupBuckets)

export default categoryGroupBuckets