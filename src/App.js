import React, {useState} from 'react';
import './App.css';
import {UpperHeader, CategoryLinks, AlphabetLinks, BottomHeader} from './components/views/views.js'

function getCookieValue(name) {
  let result = document.cookie.match("(^|[^;]+)\\s*" + name + "\\s*=\\s*([^;]+)")
  return result ? result.pop() : ""
}

var mode = getCookieValue("mode")
if (!mode) {
  mode = "categories"
  document.cookie = "mode=categories; samesite=strict; expires=Thu, 1 Jan 2099 00:00:00 UTC"
}
console.log(mode)

const App = () => {

  const [buttonText, setButtonText] = useState(mode === "categories" ? "Järjestä aakkosittain" : "Järjestä kategorioittain")
  const [arrangeMode, setarrangeMode] = useState(mode)

  return (
    <div>
      <UpperHeader buttonText={buttonText} setButtonText={setButtonText} arrangeMode={arrangeMode} setarrangeMode={setarrangeMode}/>
      { arrangeMode === "categories" ? <CategoryLinks /> : null}
      { arrangeMode === "alphabets" ? <AlphabetLinks /> : null } 
      <BottomHeader />
    </div>
  )
}

export default App;
