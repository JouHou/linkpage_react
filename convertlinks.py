import os
import json
import io

links = []

for file in os.listdir('./links'):
    with io.open(os.path.join('./links/', file), 'r', encoding='utf-8') as infile:
        lines = infile.read().splitlines()
        link = {"title": lines[0], "url": lines[1], "category": lines[2]}
        links.append(link)

with io.open('./src/components/links.js', 'w', encoding='utf-8') as outfile:
    outfile.write("const links = ")
    outfile.write(json.dumps(links, indent=2, ensure_ascii=False))
    outfile.write("\n\nexport default links")